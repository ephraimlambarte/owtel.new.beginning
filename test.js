var axios = require('axios');
const fs = require('fs');
fs.writeFile('errorlog.txt', "ERROR:\n", (err) => {  
    if (err) throw err;
});
//test get all category api
function parseResponse(response){
    var category = 0;
    var brand;
    // console.log(response);
    var _product = [];
    response.data.owtel_store_product.all_product.forEach((v, i)=>{
            
        if(typeof v != "object" && isNaN(v)){
             brand  =v;
            _product.push({
                brand:v,
                products:[]
            });
        }else if(typeof v == "object"){
            var slideobj = [];
            if(parseInt(v.data.length) >= 1){
                v.data.forEach((y, j)=>{
                   
                    if((j+1) % 4 == 0){
                        var products = {
                            product_id:y.id_product,
                            image_url:y.image_url,
                            name:y.name.split("_")[0].length > 30 ? y.name.split("_")[0].slice(0,30).toUpperCase() + "...": y.name.split("_")[0].toUpperCase(),
                            description: y.description,
                            price:y.pricesell,
                            category_id:category
                        };
                        slideobj.push(products);
                        _product[_product.length-1].products.push(slideobj);
                        slideobj = [];
                    }else{
                        var products = {
                            product_id:y.id_product,
                            image_url:y.image_url,
                            name:y.name.split("_")[0].length > 30 ? y.name.split("_")[0].slice(0,30).toUpperCase() + "...": y.name.split("_")[0].toUpperCase(),
                            description: y.description,
                            price:y.pricesell,
                            category_id:category
                        };
                        slideobj.push(products);
                    }
                    if(j+1 == v.data.length){
                        if(slideobj.length > 0){
                            _product[_product.length-1].products.push(slideobj);   
                            
                            slideobj = [];
                        }
                        
                    }
                    
                });
            }
        }else{
            category = v;
        }
    });
    _product = _product.filter(value =>value.products.length > 0);
    // console.log(_product);
    return _product;   
}
function getProductsByCategoryApi(id, categoryname){
    axios.get("http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_product_all_details?category_id="+id)
    .then((response)=>{
        console.log("\x1b[32m%s\x1b[0m", "Get All "+categoryname+" Products: OK!");
        fs.appendFile('errorlog.txt', "Get All "+categoryname+" Products: OK!\n", (err) => {  
            if (err) throw err;
        });
        if(response.data.owtel_store_product.all_product.length == 0){
            console.log("\x1b[33m%s\x1b[0m", categoryname+" Doesnt have any brands: WARNING!");
            fs.appendFile('errorlog.txt', categoryname+" Doesnt have any brands: WARNING!\n", (err) => {  
                if (err) throw err;
            });
        }
        var product = parseResponse(response); 
        if(product.length == 0){
            console.log("\x1b[33m%s\x1b[0m", categoryname+" Doesnt have any products on brands: WARNING!");
            fs.appendFile('errorlog.txt', categoryname+" Doesnt have any products on brands: WARNING!\n", (err) => {  
                if (err) throw err;
            });
        }else{
            product.forEach((v, i)=>{
                // console.log(v);
                v.products.forEach((v2, i2)=>{
                    v2.forEach((v3, i3)=>{
                        axios.get("http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_details_per_product?category_id="+v3.category_id+"&product_id="+v3.product_id)
                        .then((response)=>{
                            console.log("\x1b[32m%s\x1b[0m", "Getting "+v3.name+" details API: OK!");
                            fs.appendFile('errorlog.txt', "Getting "+v3.name+" details API: OK!\n", (err) => {  
                                if (err) throw err;
                            }); 
                        }).catch((err)=>{
                            if(err.code != "ECONNRESET" && err.code != "ETIMEDOUT" ){
                                // console.log(err.code);
                                console.log("\x1b[31m%s\x1b[0m", "Getting "+v3.name+" details API: ERROR!");
                                console.log("\x1b[31m%s\x1b[0m", "http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_details_per_product?category_id="+v3.category_id+"&product_id="+v3.product_id);
                                fs.appendFile('errorlog.txt', "Getting "+v3.name+" details API: ERROR!\n", (err) => {  
                                    if (err) throw err;
                                });
                                fs.appendFile('errorlog.txt', "http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_details_per_product?category_id="+v3.category_id+"&product_id="+v3.product_id+"\n", (err) => {  
                                    if (err) throw err;
                                });   
                            }
                        });
                    });
                });
            });
        }
    }).catch((err)=>{
        // console.log(err);
        console.log("\x1b[31m%s\x1b[0m", "Get All "+categoryname+" Products: ERROR!");
        fs.appendFile('errorlog.txt', "Get All "+categoryname+" Products: ERROR!\n", (err) => {  
            if (err) throw err;
        });
    });
}
function parsePromoResponse(response){
    var obj = [];
                response.data.data.forEach(function(v, i){
                    if(v.products.length == 0){
                        return;
                    }
                    var tempobj = {
                        category:v.name,
                        category_id:v.category_id,
                        slides:[]
                    };
                    var slides = [];
                    v.products.forEach(function(value, y){
                        // console.log(value);
                        
                        var totalamount = value.amount.replace("HK$", "");
                        totalamount = totalamount.replace(",","");
                        var displayamount = value.amount.replace("HK$", "$");
                        var percentage = "no percentage";
                        if(value.discount_value !== undefined && typeof value.discount_amount !="number"){
                            // console.log(Math.trunc((parseInt(value.discount_value)/parseInt(totalamount))*100));
                            percentage = Math.trunc((parseInt(value.discount_value)/parseInt(totalamount))*100);
                            displayamount = value.discount_amount.replace("HK$", "$");
                        }
                        value.description  = value.description.replace(/<[^>]*>/g, '');
                        value.description = value.description.replace("?", "");
                        value.description = value.description.length > 50 ? value.description.slice(0,50)+"..." : value.description;
                        var productobj = {
                            name:value.name.length > 20 ? value.name.slice(0,20).toUpperCase() + "...": value.name.toUpperCase(),
                            discountPercentage:percentage == "no percentage"? "" :percentage,
                            description:value.description,
                            amount:displayamount,
                            category_id:value.category_id,
                            product_id:value.id,
                            img_url:value.img_url[0]
                        }
                        if((y+1)%4==0){
                            slides.push(productobj);
                            tempobj.slides.push(slides);
                            slides = [];
                        }else{
                            slides.push(productobj);
                        }   
                         
                        if((y+1) == v.products.length){
                            if(slides.length >0){
                                tempobj.slides.push(slides);
                            }
                            slides = [];
                        }
                    });
                    obj.push(tempobj);
                });
    return obj;
}
axios.get("http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_all_shop_category")
    .then((response)=>{
        console.log("\x1b[32m%s\x1b[0m", "Get All Product Categories: OK!");
        fs.appendFile('errorlog.txt', "Get All Product Categories: OK!\n", (err) => {  
            if (err) throw err;
        });
    }).catch((err)=>{
        // console.log(err);
        console.log("\x1b[31m%s\x1b[0m", "Get All Product Categories: ERROR!");
        fs.appendFile('errorlog.txt', "Get All Product Categories: ERROR!\n", (err) => {  
            if (err) throw err;
        });
    });
getProductsByCategoryApi("13", "Tablet");
getProductsByCategoryApi("49", "Laptop");
getProductsByCategoryApi("12", "Mobile");
getProductsByCategoryApi("17", "Accessories");
getProductsByCategoryApi("93", "Appliances");
getProductsByCategoryApi("48", "Cameras");
getProductsByCategoryApi("117", "Creative Products");
getProductsByCategoryApi("47", "Health n Beauty");
getProductsByCategoryApi("144", "Kitchenware");
getProductsByCategoryApi("83", "Watches");
//test search api
axios.get("http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/search_product?product_search=umbrella")
    .then((response)=>{
        console.log("\x1b[32m%s\x1b[0m", "Search API: OK!");
        fs.appendFile('errorlog.txt', "Search API: OK!\n", (err) => {  
            if (err) throw err;
        });
    }).catch((err)=>{
        console.log("\x1b[31m%s\x1b[0m", "Search API: ERROR!");
        fs.appendFile('errorlog.txt', "Search API: ERROR!\n", (err) => {  
            if (err) throw err;
        });
});
//test api for geting promo products
axios.get("http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_promotioncategory_app")
    .then((response)=>{
        console.log("\x1b[32m%s\x1b[0m", "Promo category products API: OK!");
        fs.appendFile('errorlog.txt', "Promo category products API: OK!\n", (err) => {  
            if (err) throw err;
        });
        var product = parsePromoResponse(response);
        if(product.length == 0){
            console.log("\x1b[33m%s\x1b[0m", "PROMO doesnt have any products API: WARNING!");
            fs.appendFile('errorlog.txt', "PROMO doesnt have any products API: WARNING!\n", (err) => {  
                if (err) throw err;
            });
        }else{
            product.forEach((v, i)=>{
                //test getting product promo details
                v.slides.forEach((v2,i2)=>{
                    v2.forEach((v3, i3)=>{
                        
                        axios.get("http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_promotioncategory_byid_storemacau?category_id="+v3.product_id+"&product_id="+v3.product_id)
                        .then((response)=>{
                            console.log("\x1b[32m%s\x1b[0m", "Getting "+v3.name+" details API: OK!");
                            fs.appendFile('errorlog.txt', "PROMO doesnt have any products API: WARNING!\n", (err) => {  
                                if (err) throw err;
                            });
                        }).catch((err)=>{
                            if(err.code != "ECONNRESET" && err.code != "ETIMEDOUT" ){
                                console.log("\x1b[31m%s\x1b[0m", "Getting "+v3.name+" details API: ERROR!");
                                console.log("\x1b[31m%s\x1b[0m", "http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_promotioncategory_byid_storemacau?category_id="+v3.product_id+"&product_id="+v3.product_id);
                                fs.appendFile('errorlog.txt', "Getting "+v3.name+" details API: ERROR!\n", (err) => {  
                                    if (err) throw err;
                                });
                                fs.appendFile('errorlog.txt', "http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_promotioncategory_byid_storemacau?category_id="+v3.product_id+"&product_id="+v3.product_id+"\n", (err) => {  
                                    if (err) throw err;
                                });
                            }
                        });
                    });
                });
                
            });
        }
    }).catch((err)=>{
        console.log("\x1b[31m%s\x1b[0m", "Promo category products API: ERROR!");
        fs.appendFile('errorlog.txt', "Promo category products API: ERROR!\n", (err) => {  
            if (err) throw err;
        });
});

//test dr smith api
axios.get("http://test.apiowtelstorecrmmacau.owtel.com/api/owtelcrm/get_products_drsmith?category_id=62")
    .then((response)=>{
        console.log("\x1b[32m%s\x1b[0m", "Dr smith category API: OK!");
        fs.appendFile('errorlog.txt', "Dr smith category API: OK!\n", (err) => {  
            if (err) throw err;
        });
    }).catch((err)=>{
        console.log("\x1b[31m%s\x1b[0m", "Dr smith category API: ERROR!");
        fs.appendFile('errorlog.txt', "Dr smith category API: ERROR!\n", (err) => {  
            if (err) throw err;
        });
    });

// %s\x1b[0m reset
// \x1b[31m red
// \x1b[32m green
// \x1b[33m yellow