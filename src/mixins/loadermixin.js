/**For controlling the spinner loader of dadta in pages */
import axios from 'axios';
import api from '../classes/api';
import General from '../classes/general';
export default  {
    data(){
        return {
            dataLoading: true,
            dataLoadingError:false,
            nodata:false,
        }
    },
    methods:{
        gotoProduct(link){
            if(this.$route.name == "Others"){
                var date =  new Date();
                var generic = new General;
                var mydate = date.getFullYear()+"-"+(parseInt(date.getMonth()) + 1 < 10 ? "0" + (parseInt(date.getMonth()) + 1) : parseInt(date.getMonth()) + 1)+"-"+(parseInt(date.getDate()) < 10 ? "0"+date.getDate() : date.getDate())+" "+(parseInt(date.getHours()) < 10 ? "0"+date.getHours() : date.getHours())+":"+(parseInt(date.getMinutes())<10 ? "0"+date.getMinutes() : date.getMinutes())+":"+(parseInt(date.getSeconds())<10 ? "0"+date.getSeconds() : date.getSeconds());
                switch(link){
                    case '/Appliances':
                        generic.findIP.then(ip => {
                            axios.get(api.insertTracking(), {
                                params:{action: 'category',
                                location_id:"10",
                                category_id:"93",
                                category_name:"Appliances",
                                date_added:mydate,
                                host:ip
                            }})
                            .then(function (response) {
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        });
                    break;
                    case '/Healthbeauty':
                        generic.findIP.then(ip => {
                            axios.get(api.insertTracking(), {
                                params:{action: 'category',
                                location_id:"10",
                                category_id:"47",
                                category_name:"Health n Beauty",
                                date_added:mydate,
                                host:ip
                            }})
                            .then(function (response) {
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        });
                    break;
                    case '/Cameras':
                        generic.findIP.then(ip => {
                            axios.get(api.insertTracking(), {
                                params:{action: 'category',
                                location_id:"10",
                                category_id:"48",
                                category_name:"Camera",
                                date_added:mydate,
                                host:ip
                            }})
                            .then(function (response) {
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        });
                    break;
                    case '/Watches':
                        generic.findIP.then(ip => {
                            axios.get(api.insertTracking(), {
                                params:{action: 'category',
                                location_id:"10",
                                category_id:"83",
                                category_name:"Watches",
                                date_added:mydate,
                                host:ip
                            }})
                            .then(function (response) {
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        });
                    break;
                    case '/Accessories':
                        generic.findIP.then(ip => {
                            axios.get(api.insertTracking(), {
                                params:{action: 'category',
                                location_id:"10",
                                category_id:"17",
                                category_name:"Accessories",
                                date_added:mydate,
                                host:ip
                            }})
                            .then(function (response) {
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        });
                    break;
                    case '/Creativeproducts':
                        generic.findIP.then(ip => {
                            axios.get(api.insertTracking(), {
                                params:{action: 'category',
                                location_id:"10",
                                category_id:"117",
                                category_name:"Creative Products",
                                date_added:mydate,
                                host:ip
                            }})
                            .then(function (response) {
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        });
                    break;
                    case '/Kitchenware':
                        generic.findIP.then(ip => {
                            axios.get(api.insertTracking(), {
                                params:{action: 'category',
                                location_id:"10",
                                category_id:"144",
                                category_name:"Kitchenware",
                                date_added:mydate,
                                host:ip
                            }})
                            .then(function (response) {
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        });
                    break;
                }
                
                var that = this;
                setTimeout(function(){
                    that.$router.push(link);
                    console.log
                }, 500);
                
            }else{
                this.$router.push(link);
            }
            
        }
    }
  }