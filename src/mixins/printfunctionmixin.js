/**This mixin can be used to all views except the home.vue because it uses a different api for getting product details */
import axios from 'axios';
import General from '../classes/general';
import api from '../classes/api';
export default  {
    methods:{
        printItem(categoryid, productid){
            document.location = 'printhandler:'+categoryid+'?'+productid+'?category';
            this.loadingPrint = ""; 
            this.loadingPrintStyle = "none;";        
            this.$refs.activate.showModal();
            this.loadingPrint = "load-complete"; 
            this.loadingPrintStyle = "block;";

            var that =this;
            setTimeout(function(){
                that.$refs.activate.hideModal();
            }, 5000);
            var date =  new Date();
            var mydate = date.getFullYear()+"-"+(parseInt(date.getMonth()) + 1 < 10 ? "0" + (parseInt(date.getMonth()) + 1) : parseInt(date.getMonth()) + 1)+"-"+(parseInt(date.getDate()) < 10 ? "0"+date.getDate() : date.getDate())+" "+(parseInt(date.getHours()) < 10 ? "0"+date.getHours() : date.getHours())+":"+(parseInt(date.getMinutes())<10 ? "0"+date.getMinutes() : date.getMinutes())+":"+(parseInt(date.getSeconds())<10 ? "0"+date.getSeconds() : date.getSeconds());
            axios.get(api.getProductDetails(categoryid, productid))
                .then((response)=>{
                    var general = new General;
                    general.findIP.then(ip => {
                        axios.get(api.insertTracking(), {
                            params:{action: 'print',
                            location_id:"10",
                            product_code:response.data[0][0].product_code,
                            product_name:response.data[0][0].name,
                            date_added:mydate,
                            host:ip
                        }})
                        .then(function (response) {
                            console.log(response);
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    });
                });
        }
    }
}