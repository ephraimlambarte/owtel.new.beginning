/**Everything imported here will be imported in each components */
import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'font-awesome/css/font-awesome.css';
import './assets/styles/custom.css';
import './assets/styles/loader.css';
/**Import router for urls */
import router from './router';
import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel);

Vue.use(BootstrapVue);

/**Uncomment this next line if you want right click to work on website */
// document.addEventListener('contextmenu', event => event.preventDefault());

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
