import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/views/Home'
import Mobile from '../components/views/Mobile'
import Laptop from '../components/views/Laptop'
import Tablet from '../components/views/Tablet'
import Others from '../components/views/Others'
import Product from '../components/views/Product'
import Appliances from '../components/views/others/appliances'
import HealthBeauty from '../components/views/others/healthbeauty'
import Cameras from '../components/views/others/cameras'
import Watches from '../components/views/others/watches'
import Accessories from '../components/views/others/accessories'
import Creativeproducts from '../components/views/others/creativeproducts'
import Kitchenware from '../components/views/others/kitchenware'
import Search from '../components/views/search'
import Drsmith from '../components/views/drsmith'
Vue.use(Router)

export default new Router({
    routes: [
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
          path:'/mobile',
          name:'Mobile',
          component:Mobile
      },
      {
        path:'/laptop',
        name:'Laptop',
        component:Laptop
      },
      {
        path:'/tablet',
        name:'Tablet',
        component:Tablet
      },
      {
        path:'/others',
        name:'Others',
        component:Others
      },
      {
        path:'/product',
        name:'Product',
        component:Product
      },
      {
        path:'/appliances',
        name:'Appliances',
        component:Appliances
      },
      {
        path:'/healthbeauty',
        name:'Healthbeauty',
        component:HealthBeauty
      },
      {
        path:'/cameras',
        name:'Cameras',
        component:Cameras
      },
      {
        path:'/watches',
        name:'Watches',
        component:Watches
      },
      {
        path:'/accessories',
        name:'Accessories',
        component:Accessories
      },
      {
        path:'/creativeproducts',
        name:'Creativeproducts',
        component:Creativeproducts
      },
      {
        path:'/kitchenware',
        name:'Kitchenware',
        component:Kitchenware
      },
      {
        path:'/search',
        name:'Search',
        component:Search
      },
      {
        path:'/drsmith',
        name:'Drsmith',
        component:Drsmith
      },
      
      
    ]
  })