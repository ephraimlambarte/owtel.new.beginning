import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import * as qz from 'qz-tray';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
// import $ from 'jquery';

export default class General{
    _product = [];
    findIP = new Promise(r=>{var w=window,a=new (w.RTCPeerConnection||w.mozRTCPeerConnection||w.webkitRTCPeerConnection)({iceServers:[]}),b=()=>{};a.createDataChannel("");a.createOffer(c=>a.setLocalDescription(c,b,b),b);a.onicecandidate=c=>{try{c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r)}catch(e){}}})
    index(){
        console.log("Hello Mixin");
    }
    viewPdf(mobilename, amount, productcode,image){
       
        var dd = {
            pageSize: {
                width: 130,
                height: 600
            },
            header:{
                margin:[ 5, 5, 5, 15 ], 
                columns:[
                        {
                            width: 'auto',
                            style:'table',
                            table:{
                                headerRows: 2,
                                widths:[115],
                                body:[ 
                                        [
                                            {
                                                image:  this.getLogo(),
                                                width: 80,
                                                alignment:'center'
                                            }
                                        ],
                                        [
                                            {
                                                text:'MACAU',
                                                style:'secondheader',
                                                alignment:'center',
                                                // margin: [ 5, 0, 5, 15 ] 
                                            },
                                        ]
                                     ]
                            },
                            alignment:'center',
                            layout: 'noBorders'
                        },
                        { width: '*', text: '' },
                    
                ]
            },
            content: [
            {
                columns: [
                    { width: '*', text: '' },
                    {
                        width: 'auto',
                        style:'table',
                        table:{
                            widths:[55, 'auto'],
                            body:[ 
                                  [{text:'Product Code:', style:'label', alignment:'left'}, 
                                   {text:productcode, alignment:'left'}],
                                  [{text:'Product Name:', style:'label', alignment: 'left'}, 
                                   {text: mobilename.toUpperCase(),alignment: 'left'}],
                                  [{text:'Cash Price:', style:'label', alignment:'left'},
                                    {text:"MOP"+this.formatNumber(amount.replace("MOP", "")), alignment:'left'}
                                    ]
                                 ]
                        },
                        alignment:'center',
                        layout: 'noBorders'
                    },
                    { width: '*', text: '' },
                ]
            },  
            {
                image: image,
                width: 100,
                height:100,
                alignment:'center',
                margin: [ 5, 2, 5, 20 ] 
            },
            {
                text:['Customer Service: 999-444-222-2548\n',
                    'Website:www.owtelstore.com\n'],
                style:'footerRegular',
                alignment:'center',
                margin: [ 5, 2, 5, 20 ] 
            },
            { text: 'Thank You!', 
                      alignment: 'center',
                      style:"thankyou",
                      margin: [ 0, 0, 0, 30 ] 
                    }
        ],
        styles: {
            header: {
                fontSize: 18,
                bold: true
            },
            secondheader:{
                fontSize:12
            },
            regular: {
                fontSize: 10
            },
            thankyou:{
                fontSize:9,
                bold:true
            },
            footerRegular:{
                fontSize:9,
            },
            label:{
                bold:true
            },
            table:{
                margin: [ 5, 2, 5, 20 ],
                fontSize: 8 
            },
            mop:{
                fontSize: 5
            }
        },

        pageOrientation: 'portrait',

        pageMargins: [ 0, 80, 0, 20 ],
        }
        // pdfMake.createPdf(dd).open();
        // pdfMake.createPdf(dd).print({}, window);
        pdfMake.createPdf(dd).print();
    }
    printPDF(){
        window.frames.pdfFrame.print();
    }
    printStuff() {
        qz.websocket.connect().then(function() {
            alert("Connected!");
        }).catch((err)=>{
             alert("ERror!");
        });
        qz.printers.find("epson").then(function(found) {
            alert("Printer: " + found);
        });
     }
    formatNumber(number){
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    checkPriceSell(value){
        switch(typeof value){
            case "string":
                return Math.ceil(parseInt(value.replace(/\D/g,'')));
            break;
            case "number":
                return Math.ceil(value);
            break;
            default:
                return "";
        }
    }
    parseResponse(response){
        var category = 0;
        var brand;
        // console.log(response);
        var namelimitter = 43;
        response.data.owtel_store_product.all_product.forEach((v, i)=>{
                
            if(typeof v != "object" && isNaN(v)){
                 brand  =v;
                this._product.push({
                    brand:v,
                    products:[]
                });
            }else if(typeof v == "object"){
                var slideobj = [];
                if(parseInt(v.data.length) >= 1){
                    v.data.forEach((y, j)=>{
                        y.pricesell = "$"+this.formatNumber(this.checkPriceSell(y.pricesell));
                        
                        /**change the number after modulus to change number of product per slide
                         * 
                         * just make sure you adjusted the grid layout to match with number of products per slide
                        */
                        y.description =y.description.replace(/<[^>]*>/g, '');
                        y.description = y.description.replace("?", "");
                        y.description =y.description.length > 40 ? y.description.slice(0,40)+"..." : y.description;
                        
                        if((j+1) % 4 == 0){
                            var products = {
                                product_id:y.id_product,
                                image_url:y.image_url,
                                name:y.name.split("_")[0].length > namelimitter ? y.name.split("_")[0].slice(0,namelimitter).toUpperCase() + "...": y.name.split("_")[0].toUpperCase(),
                                description: y.description,
                                price:y.pricesell,
                                category_id:category
                            };
                            slideobj.push(products);
                            this._product[this._product.length-1].products.push(slideobj);
                            slideobj = [];
                        }else{
                            var products = {
                                product_id:y.id_product,
                                image_url:y.image_url,
                                name:y.name.split("_")[0].length > namelimitter ? y.name.split("_")[0].slice(0,namelimitter).toUpperCase() + "...": y.name.split("_")[0].toUpperCase(),
                                description: y.description,
                                price:y.pricesell,
                                category_id:category
                            };
                            slideobj.push(products);
                        }
                        if(j+1 == v.data.length){
                            if(slideobj.length > 0){
                                this._product[this._product.length-1].products.push(slideobj);   
                                
                                slideobj = [];
                            }
                            
                        }
                        
                    });
                }
            }else{
                category = v;
            }
        });
        this._product = this._product.filter(value =>value.products.length > 0);
        // console.log(this._product);
        return this._product;   
    }
    getLogo() {
        var dataurl = "data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAAA8CAYAAAD2SSHcAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkNFOTkzQjUxOEU0MDExRThCOEMxQzY5NTFFNDRDOTEzIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkNFOTkzQjUyOEU0MDExRThCOEMxQzY5NTFFNDRDOTEzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Q0U5OTNCNEY4RTQwMTFFOEI4QzFDNjk1MUU0NEM5MTMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Q0U5OTNCNTA4RTQwMTFFOEI4QzFDNjk1MUU0NEM5MTMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4KfXLQAAAIKklEQVR42uxdC2wVRRSdBwULlE8FkY9CE5EiCmgEFERoJCgalRoIgmgiRIkSRDFqRAU/IWLUGERrYmJCg4DEaEQbDcFIwT/4I35Q6o8AtlD8UASEAtZ7fffFl/XO7s7s7L73mnuSk5J5+2Zn9p29c++dD6mWlhYlEOQ72sgjEIhQBQIRqkCEKhCIUAUCEapAhCoQiFAFAhGqoPBRFLWCVCqFfwYDJwBHAwcB+wG70SVHgb8DtwO/BNYSmxy0/1xiNvYA11nU1Qs4kSlfDWy2qO86YHtP2QbgTvr3FGBJjL/tNuAWB/V0Bw4A9gB2Anb0fP4K8KCnrIT6p4KuDT0zihfakBpzJ/ArrMaQfwHXAMdFfIgPMXVvtKyrQtPWbpb17Wfqqsz6fIfFczPhUst2nwy8CfgyvfRB9ylj6igLe21YvRVZWND2JNC76G2zQTHwWuImqutTGeByChyZ7gFOZkaCwvJRQaQj4c9W4JIIIvUCrepm4DISsCBZ9AauBH4BnJ6PIjUSKoh0Lvx5H3hWTO24DfixZigRxINryJedUfDBVCodLT1Gw4If0FetAX4A/JYCKPRFOwP709AyHni1TxAxjL4/keprzVgP7BlwDQYwFzHlr4eoP+j5PQxcFLKth4E/0G/axHwWP0IETUsCnOm1wBEGt8TI8RbgLp869wEHtvJgKkqbomJZiCAJDQaOomejvTKs33kwFSTSW306spNSUrZAq/qMT/07KAIVoboV6gMBAn2VSfmpvBUq4HyVzoFyN3wXeIqj3QFTfe5TE/A2i1DNcCnwb02dDYrPI+eFUNto/FL0Xas1ESCmkybCl/c56hTm6y4HHmM+uxJ4o8Q8TtAVuELz4n9GVnRdvjZeF/VjBH4OU16HVgFE6tqBxhmb2ZrPHo9g1QT/4RHgqUz5Fgpy9+Zz49sw1rQD/FnAXHsc82wg0v0xtQUt+CpN5Hu76CwS+lK84cUuGrWa8r0DnEWdhf4nU14FIv085vbMBx7QWPiTRG/WuAPYzuseAq+nDIsqRKFyPiHmQxcn0B58aNwcNc6CTRK9WaEtCdKLVRQUq4ITKgz7Z8Cf4VynwJr+mlCbqsjN8GKaaM4KY1V6ZZgXiwupE16LqsuLrk6wTY3Ad5jyCiXrZ21wCVOGyfzthSzU0cw1h4DvJdyut5iyUpWeJRGYYRRTVltonfDO9Q9mrvkEhv3jCbfrI015uWr9awBco5wpw/UWQxzVP5tGwUSFWsZcU5eDh6u755miO+Pf9zSmfCjRVUYh8aGfS6w35OABY17vKFNeItozQqt5Xl6htmWuac5R244wZZ1Ee0bo0lqFeoK5Jlcrvos1gZ0gPA60Jh8mGzg96t1i0jsH7cIFFNxM1EHRnhF0z+sF4JuO7tGYC6HuYIQ6MAcPWHfP70V7RsBszW4moMLJm7WFPPRvY64ZQcv+ksQoTfl20Z4xuGd2XqH7qB9qAphxCbfrCqbsD+A3ojtjcL/pGGCHQhbq25rrpifYJtzwNp4p36jSq9MFZqjVGJ/KHLSlyIlQW1paflT8QRAzYPjvkVBn5mo6tEY0ZwVcIcXlwjFRn0q4LZ1dWVTEcqYMU0ULE7Km3CLp31S4LcKZtuYD8uUgB0w5rmTK8TCRqTHd85imvJdroXKLaeeAVR0e80PFtahckhp3q3IzVdyWmFLLe5c4tALFGh/wzxyJdalGPM8qfntKVDRq3LQhzoQKwz8ukl6i8S9Wg1hLY3qYMzW+MKZSntZ8hxvS+ltaM11KrNyirgGa8j05Emq9Sq/z9QLdudfU/0/oc2FRf2bKK6xr1GyVRlHqTunDoKZj1rUucJlKT9Vy95vp873hmu+MtWjDG5q6bBYYz2PqabZwSyqUu+3SXenF5urboNyvC3iRuQ+6IX2D9Od6X39PR0Kd5iPSoH39+EI1Md97ybANZSqdHOfagNbIZL8WjlJfM/XYrOl1KVTEBKXf149n1w5yKNQpmvusdCrUECel7CZLGCUCrFLRTkpBrNB8P2zb8EVYp/xPD3nCoF93a+qYnwdCRSzw6Sf6/IscWdf2Sn++6sKMAXIiVIOzpy4wDFrm+QxDJmdPIUZo6mgKIVZsS7UKdzDu/QF5QHzwc2h4834X59y75YlQM8GVX1/xMLSnVHqGMMqs5Dyfe+B2Izy5pTiMUFNBw7fBaX7byM/L7MfZR051Jwpw8KQ+TORfpfyX6+FQa3qaHx65PVnzGW5rwR2XmykaxcChH91jjmHKBE8pfJ4ecgOJsjf5xDeTu8QBRf6opVBrNS9FVDyo0kciBQGtbB39nlyWxW+FfxG5PBf61I/B+3eUgjzExFCVZj5COhHfrOI9znursjsfFcW211Eb6ulNP+KoPjyJpJ2lmOKyqBlMIusZpX9Bv9fp5CZa1R966PeIdSRZTtcCRcsU9cRpbNuBiO1oonoUpcpORKwPzxTtE6FPcQs185KviFGomXRdXWJCJbGik3wv5TddiHSj4s8SsAEOvTst24GO/8VMRuJwBEvaN2J/khBqBkMpIj8ag1AzwfNzPtkVt0LNs/8VhUMpZRPCuimYrlnuM0ODO3PXG/QNF5/fp9xMoSYp1Ox86yz6feodCjXbuj4J/MlEqKmoudAc/z9TfkBrdgP5m8OyUl34Ru8iF2YT5Vx3h6gPTzfEufExlJHokxXU/EL+dQ39wK76pvu/r6pVciglcXWnQLQLE8gejNC/crpHVy5IBH3+29eUw9klgSA2yBE5AhGqQCBCFYhQBQIRqkAgQhWIUAUCEapA4I9/BBgAqmplAISSZdgAAAAASUVORK5CYII="
        return dataurl;
    }
    parseSingleProduct(response){
        // console.log(response);
        var product = {
            product_id: response.data[0][0].id_product,
            name: response.data[0][0].name.split("_")[0],
            amount: "MOP$"+ response.data[0][0].pricesell.split(".")[0],
            description: response.data[0][0].description,
            image_url:response.data.images[0],
            downpayment: response.data[0][0].DP6 == "" &&
                        response.data[0][0].month6 == "" ? false: true,
            dp6: response.data[0][0].DP6,
            month6: Math.ceil(response.data[0][0].month6),
            product_code:response.data[0][0].product_code
        }
        product.description =product.description.replace(/\?/g, ' ');
        product.image_url.forEach((v, i)=>{
            product.image_url[i].image_url = "data:image/jpg;base64,"+product.image_url[i].image_url
        });
        return product;
    }
    searchProductParser(response){
    
        var obj = [];
        var slideTemp = [];
        response.data.data.forEach((v, i)=>{
            var tempobj = {};
            v.pricesell = "$"+this.formatNumber(this.checkPriceSell(v.pricesell));
            
            // v.meta_keywords =v.meta_keywords.slice(0,50) + "...";
            v.description =v.description.replace(/<[^>]*>/g, '');
            v.description =v.description.slice(0,50) + "...";
            if((i+1)%4 == 0){
                tempobj = {
                    product_id:v.id_product,
                    image_url:v.image_url,
                    description:v.description,
                    name:v.name,
                    amount:v.pricesell,
                    category_id:0
                }
                slideTemp.push(tempobj);
                obj.push(slideTemp);
                slideTemp = [];
            }else{
                tempobj = {
                    product_id:v.id_product,
                    image_url:v.image_url,
                    description:v.description,
                    name:v.name,
                    amount:v.pricesell,
                    category_id:v.id_category
                }
                slideTemp.push(tempobj);
            }
            if((i+1) == response.data.data.length){
                obj.push(slideTemp);
                slideTemp = [];
            }
        });
        return obj;
    }
}
