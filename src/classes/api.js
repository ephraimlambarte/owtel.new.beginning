export default class api{
    /** API URLS */
    
        static getAllCategoryid(){
            return "//api.owtelstorecrmmacau.owtel.com/api/owtelcrm/get_all_shop_category";
        }
        static getProductsByCategoryId(categoryid){
            return "//api.owtelstorecrmmacau.owtel.com/api/owtelcrm/get_product_all_details?category_id="+categoryid;
        }
        static getProductDetails(categoryid, productid){
            return "//api.owtelstorecrmmacau.owtel.com/api/owtelcrm/get_details_per_product?category_id="+categoryid+"&product_id="+productid;
        }
        static getProductDetailsPromo(categoryid, productid){
            return "//api.owtelstorecrmmacau.owtel.com/api/owtelcrm/get_promotioncategory_byid_storemacau?category_id="+categoryid+"&product_id="+productid
        }
        static getPromoProducts(){
            return "//api.owtelstorecrmmacau.owtel.com/api/owtelcrm/get_promotioncategory_app";
        }
        static searchProducts(search){
            return "//api.owtelstorecrmmacau.owtel.com/api/owtelcrm/search_product?product_search="+search;
        }
        static getDrSmithProducts(){
            return "//api.owtelstorecrmmacau.owtel.com/api/owtelcrm/get_products_drsmith?category_id=62";
        }
        static insertTracking(){
            return "//api.owtelstorecrmmacau.owtel.com/api/owtelcrm/insert_preferences";
        }
}